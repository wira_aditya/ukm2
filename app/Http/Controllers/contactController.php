<?php

namespace App\Http\Controllers;

Use Validator;
use App\contact;
use Illuminate\Http\Request;

class contactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = contact::all();
        return view('index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required'    => ':attribute tidak boleh kosong',
            'numeric' => ':attribute harus berupa angka',
            'email'      => 'format email salah',
            'msg.min'      => 'pesan harus lebih dari :min karakter',
            'msg.max'      => 'pesan harus kurang dari :max karakter',
            'max'      => 'pesan harus kurang dari :max karakter',
        ];
        $rules = [
                'nama' =>'bail|required|string|max:255',
                'no_hp' =>'bail|required|numeric',
                'email' =>'bail|required|email|max:255',
                'msg' =>'bail|required|min:30|max:255',
            ];

        $validator = Validator::make($request->all(), $rules, $messages);
        // $this->validate($request,[
        //         'nama' =>'bail|required|string|max:255',
        //         'no_hp' =>'bail|required|integer',
        //         'email' =>'bail|required|email|max:255',
        //         'msg' =>'bail|required|min:30|max:255',
        //     ]);
        if ($validator->fails()) {
            return redirect('contact/create')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $contact = new contact;
            $contact->nama = $request->nama;
            $contact->no_hp= $request->no_hp;
            $contact->email= $request->email;
            $contact->msg= $request->msg;
            $contact->save();
            return redirect('contact');
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = contact::findOrFail($id);
        return view('edit',compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required'    => ':attribute tidak boleh kosong',
            'numeric' => ':attribute harus berupa angka',
            'email'      => 'format email salah',
            'msg.min'      => 'pesan harus lebih dari :min karakter',
            'msg.max'      => 'pesan harus kurang dari :max karakter',
            'max'      => 'pesan harus kurang dari :max karakter',
        ];
        $rules = [
                'nama' =>'bail|required|string|max:255',
                'no_hp' =>'bail|required|numeric',
                'email' =>'bail|required|email|max:255',
                'msg' =>'bail|required|min:30|max:255',
            ];

        $validator = Validator::make($request->all(), $rules, $messages);
        // $this->validate($request,[
        //         'nama' =>'bail|required|string|max:255',
        //         'no_hp' =>'bail|required|integer',
        //         'email' =>'bail|required|email|max:255',
        //         'msg' =>'bail|required|min:30|max:255',
        //     ]);
        if ($validator->fails()) {
            return redirect('contact.create')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            
            $contact = contact::findOrFail($id);
            $contact->nama = $request->nama;
            $contact->no_hp= $request->no_hp;
            $contact->email= $request->email;
            $contact->msg= $request->msg;
            $contact->save();
            return redirect('contact');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = contact::findOrFail($id);
        $contact->delete();
        return redirect()->route('contact.index');
    }
}
