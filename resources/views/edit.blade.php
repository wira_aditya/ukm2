@extends('layouts.app')
@section('content')
<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		<div class="panel-heading"><h2>Insert Data</h2></div>
		<div class="panel-body">
			{!! Form::model($contact, ['route'=>['contact.update', $contact->id], 'method'=> 'PATCH']) !!}
			<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
		        {{ Form::label('nama', 'Nama') }}
		        {{ Form::text('nama', null, ['class' => 'form-control','required' => 'required']) }}
		        <p class="help-block">{{ $errors->first('nama', ':message') }}</p>
			</div>
				
			<div class="form-group{{ $errors->has('no_hp') ? ' has-error' : '' }}">
				{{ Form::label('nohp', 'No Hp') }}
		        {{ Form::text('no_hp', null, ['class' => 'form-control','required' => 'required']) }}
		        <p class="help-block">{{ $errors->first('no_hp', ':message') }}</p>
			</div>
				
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				{{ Form::label('email', 'E-Mail') }}
		        {{ Form::text('email', null, ['class' => 'form-control','required' => 'required']) }}
		        <p class="help-block">{{ $errors->first('email', ':message') }}</p>
			</div>

			<div class="form-group{{ $errors->has('msg') ? ' has-error' : '' }}">
				{{ Form::label('msg', 'Pesan') }}
		        {{ Form::textarea('msg', null, ['class' => 'form-control','required' => 'required']) }}
		        <p class="help-block">{{ $errors->first('msg', ':message') }}</p>
			</div>
			    {!! Form::submit('Simpan',['class'=>'btn btn-success']) !!}
			    {!! Form::reset("Reset",["class"=>"btn btn-danger"]) !!}
		</div>
	</div>
</div>
@endsection