
@extends('layouts.app')
@section('content')
<div class="col-md-10 col-md-offset-1">
    <a href="{{ route('contact.create') }}" class="btn btn-success" style="margin: 30px 0;">Tambah Data</a>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Telp</th>
                <th>Email</th>
                <th>Pesan</th>
                <th>Aksi</th>
            </tr>
            @php($no=1)
            @foreach($data as $value)
            
            <tr>
                <td class="break" style='min-width:50px; max-width:50px'>{{$no++}}</td>
                <td class="break" style='min-width:220px; max-width:220px'>{{$value->nama}}</td>
                <td class="break" style='min-width:150px; max-width:150px'>{{$value->no_hp}}</td>
                <td class="break" style='min-width:200px; max-width:200px'>{{$value->email}}</td>
                <td class="break" style="max-width: 250px">{{$value->msg}}</td>
                <td style='min-width:180px; max-width:180px' align="center">
                    <!-- <a  href="{{route('contact.destroy', $value->id) }}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i>
                    </a> -->
                <form method="POST" action="{{ route('contact.destroy', $value->id) }}" accept-charset="UTF-8">
                    <a class="btn btn-warning" href="{{ route('contact.edit', $value->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                    <input name="_method" type="hidden" value="DELETE">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <button onclick="return confirm('Anda yakin akan menghapus data ?');" type="submit" button type="button" class="btn btn-danger" value="Hapus">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </a>
                </form>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection