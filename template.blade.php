<!DOCTYPE html>
<html lang="en">
<head>
	<style>
		.padCtn{
			padding-top:50px; 
		}
		th{
			background-color: #eaeaea;
			text-align: center !important;
		}
	</style>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">	
	<meta charset="UTF-8">
	<title><i>Ini Title</i></title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
	<div class="container-fluid padCtn">
		<div class="col-md-10 col-md-offset-1">
			<button class="btn btn-success" style="margin: 30px 0;">Tambah Data</button>
			<div class="table-responsive">
				<table class="table">
					<tr>
						<th width="50px">No</th>
						<th>Nama</th>
						<th>Telp</th>
						<th width="250px">Email</th>
						<th>Pesan</th>
						<th width="180px">Aksi</th>
					</tr>
					<tr>
						<td>1</td>
						<td>wira</td>
						<td>081</td>
						<td>wiraaditya131@gmail.com</td>
						<td>hai</td>
						<td align="center">
							<button class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</button>
							<button class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i>
							</button>
							<button class="btn btn-info"><i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</td>
					</tr>
					<tr>
						<td>1</td>
						<td>wira</td>
						<td>081</td>
						<td>wiraaditya131@gmail.com</td>
						<td>hai</td>
						<td align="center">
							<button class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</button>
							<button class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i>
							</button>
							<button class="btn btn-info"><i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</td>
					</tr>
					<tr>
						<td>1</td>
						<td>wira</td>
						<td>081</td>
						<td>wiraaditya131@gmail.com</td>
						<td>hai</td>
						<td align="center">
							<button class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</button>
							<button class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i>
							</button>
							<button class="btn btn-info"><i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>